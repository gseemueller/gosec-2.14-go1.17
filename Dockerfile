FROM registry.gitlab.com/gitlab-org/security-products/analyzers/gosec:2.14.0 as original-analyzer


FROM golang:1.17-alpine

ENV CGO_ENABLED=0 GOOS=linux GO111MODULE=auto
RUN apk --no-cache add git ca-certificates gcc libc-dev && \
    mkdir /.cache && \
    chmod -R g+r /.cache

COPY --from=original-analyzer /analyzer /analyzer
COPY --from=original-analyzer /bin/gosec /bin/gosec

ENTRYPOINT []
CMD ["/analyzer", "run"]
